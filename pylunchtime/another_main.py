try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract
import cv2
import os

pytesseract.pytesseract.tesseract_cmd = r'/usr/bin/tesseract'

imageFilePath = '../Vecka+37+Sthlm+Street+Lunch+2019++(kopia+5).jpg'

image = cv2.imread(imageFilePath)
grayScaleImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
invertedColorsImage = cv2.bitwise_not(grayScaleImage)

filename = "{}.png".format(os.getpid())
cv2.imwrite(filename, invertedColorsImage)

text = pytesseract.image_to_string(Image.open(filename), lang='swe')
# os.remove(filename)
print(text)


# print(pytesseract.image_to_string(img, lang='swe'))
# print(pytesseract.image_to_string(Image.open(imageFilePath), lang='swe'))
